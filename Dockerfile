# pull image ubuntu from docker hub
FROM ubuntu:16.04

# add maintainer info
LABEL maintainer="hendra@portcities.net"

# add some environment for ubuntu
ENV LANG C.UTF-8
ENV DEBIAN_FRONTEND=noninteractive

# run command in ubuntu container
RUN apt update && apt upgrade -y
RUN apt install -y wget python3 python3-dev python3-pip postgresql-client
RUN pip3 install --upgrade pip
RUN pip3 install phonenumbers num2words

# download wkhtmltox and install
RUN wget https://downloads.wkhtmltopdf.org/0.12/0.12.5/wkhtmltox_0.12.5-1.xenial_amd64.deb
RUN apt install -y ./wkhtmltox_0.12.5-1.xenial_amd64.deb
RUN rm -f wkhtmltox_0.12.5-1.xenial_amd64.deb

# download new odoo 12 community nightly and install
RUN wget https://nightly.odoo.com/12.0/nightly/deb/odoo_12.0.latest_all.deb
RUN apt install -y ./odoo_12.0.latest_all.deb
RUN rm -f odoo_12.0.latest_all.deb

# if you have extra requirements, you can create new file ex. extra_requirements.txt, then add commands below
# COPY ./extra_requirements.txt
# RUN pip3 install -r extra_requirements.txt

# Set the default config file
ENV ODOO_RC /etc/odoo/odoo.conf

# Set default user when running the container
USER odoo

CMD ["odoo"]
