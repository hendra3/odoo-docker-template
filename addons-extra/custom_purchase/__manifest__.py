# -*- coding: utf-8 -*-

{
    'name': 'Custom Purchase',
    'version': '0.1',
    'category': 'Operations/Purchase',
    'summary': 'Custom Purchase',
    'description': "",
    'website': 'https://www.odoo.com/page/purchase',
    'depends': ['purchase'],
    'data': [
        'views/res_partner_views.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
